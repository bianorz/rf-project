// Projeto: Receptor RF
// Autor: Bianor Galdino (Atualizado por Tales Vieira)
// Data: 18/06/18
// Versão 0.1.0
// Descricao:
/* O projeto tem por objetivo receber um comando por meio da porta serial e acionar uma saida de acordo
 *  com o comando
 */
/*
 To Do: Change D13 pin BUG , Otimization

 */

#include <avr/wdt.h>
#include "var_global.h"

void pulso_rele(int rele){
  digitalWrite(rele, 0);
  delay(500);
  digitalWrite(rele, 1);
}

void send_states() {

  e_chave1 = digitalRead(chave1);

  if (e_chave1 == 0) {
    quadro1 = 0;
  }

  delay(5);
  e_chave2 = digitalRead(chave2);

  if (e_chave2 == 0) {
    quadro2 = 0;
  }

  delay(5);
  e_chave3 = digitalRead(chave3);

  if (e_chave3 == 0) {
    quadro3 = 0;
  }

  delay(5);
  e_chave4 = digitalRead(chave4);

  if (e_chave4 == 0) {
    quadro4 = 0;
  }

  delay(5);
  e_chave5 = digitalRead(chave5);

  if (e_chave5 == 0) {
    quadro5 = 0;
  }

  delay(5);
  e_chave6 = digitalRead(chave6);

  if (e_chave6 == 0) {
    quadro6 = 0;
  }

  delay(5);

  data[0] = 0xbb;
  data[1] = quadro1 * pow(2, 0) + (!e_chave1) * pow(2, 1)
      + e_chave1 * pow(2, 2) +

      quadro2 * pow(2, 3) + (!e_chave2) * pow(2, 4)
      + e_chave2 * pow(2, 5);

  data[2] = quadro3 * pow(2, 0) + (!e_chave3) * pow(2, 1)
      + e_chave3 * pow(2, 2) +

      quadro4 * pow(2, 3) + (!e_chave4) * pow(2, 4)
      + e_chave4 * pow(2, 5);

  data[3] = quadro5 * pow(2, 0) + (!e_chave5) * pow(2, 1)
      + e_chave5 * pow(2, 2) +

      quadro6 * pow(2, 3) + (!e_chave6) * pow(2, 4)
      + e_chave6 * pow(2, 5);

  Serial.write(data[0]);
  Serial.write(data[1]);
  Serial.write(data[2]);
  Serial.write(data[3]);
}

int dataReceived[8] = { };

void setup() {
  // Inicializacao do Watchdog Timer
  wdt_enable(WDTO_4S);

  //Deficao dos reles como saidas e chaves como entradas
  /*
   pinMode(a_rele1, OUTPUT);
   pinMode(a_rele2, OUTPUT);
   pinMode(a_rele3, OUTPUT);  // RELES ACIONAMENTO
   pinMode(a_rele4, OUTPUT);
   pinMode(a_rele5, OUTPUT);
   pinMode(a_rele6, OUTPUT);

   pinMode(d_rele1, OUTPUT);
   pinMode(d_rele2, OUTPUT);
   pinMode(d_rele3, OUTPUT);  // RELES DESACIONAMENTO
   pinMode(d_rele4, OUTPUT);
   pinMode(d_rele5, OUTPUT);
   pinMode(d_rele6, OUTPUT);
   */
// Definição dos relés (acionamento e desacionamento) como saida
  for (int i = 2; i <= 12; i++) {
    pinMode(i, OUTPUT);
  }
  pinMode(a_rele6, OUTPUT); // a_rele6 = 14, devido ao BUG da porta 13

// Definição das chaves(relés 220V) como entrada
  for (int i = 15; i <= 19; i++) {
    pinMode(i, INPUT);
  }
  pinMode(chave1, INPUT); // chave1 = 13, devido ao BUG da porta 13

// Desacionamento dos relés de acionamento e desacionamento
  for (int i = 2; i <= 12; i++) {
    digitalWrite(i, 1); // #Observação: O segundo argumento da função 'digitalWrite()' foi alterado de 0 para 1. (Motivo da alteração: Os relés do 'Módulo Relé 16 Canais' são desacionados em nível lógico 1)
  }
  digitalWrite(a_rele6, 1); // a_rele6 = 14, devido ao BUG da porta 13. #Observação: O segundo argumento da função 'digitalWrite()' foi alterado de 0 para 1. (Motivo da alteração: Os relés do 'Módulo Relé 16 Canais' são desacionados em nível lógico 1)

  // Inicializacao da porta serial
  Serial.begin(9600);

  // Enviar comando informando que todos os reles estao desacionados
  Serial.write(0xcc);
}

void loop() {
  // Resetar o watchdog timer
  wdt_reset();

  // Enviar estado dos leds
  send_states();

  delay(500);
  //Verificar se existe dado na serial
  while (Serial.available() > 0) {

    // Ler esse dado como um comando
    cmd = Serial.read();

    // Comparar os diferentes comandos possíveis
    switch (cmd) {

    case 0xa0:
      for (int i = 1; i <= 7; i++) {
        dataReceived[i] = Serial.read();
      }

      if (dataReceived[7]
          == ((dataReceived[6] >= 1) + (dataReceived[5] >= 1) + (dataReceived[4] >= 1)
              + (dataReceived[3] >= 1) + (dataReceived[2] >= 1) + (dataReceived[1] >= 1))) {
//BOTAO 1------------------------------------------
        if (dataReceived[1] >= 1) {
          if (e_chave1 == HIGH) {
            if (quadro1 == 0) {
              pulso_rele(a_rele1); // #Observação: A função 'pulso_rele()' com o argumento 'a_rele1' foi alterada para função 'pulso_rele()' com o argumento 'd_rele1'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro1 = 1;
            }
          }
        } else {
          if (e_chave1 == HIGH) {
            if (quadro1 == 1) {
              pulso_rele(d_rele1); // #Observação: A função 'pulso_rele()' com o argumento 'd_rele1' foi alterada para função 'pulso_rele()' com o argumento 'a_rele1'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro1 = 0;
            }
          }
        }

//BOTAO 2---------------------------------------
        if (dataReceived[2] >= 1) {
          if (e_chave2 == HIGH) {
            if (quadro2 == 0) {
              pulso_rele(a_rele2); // #Observação: A função 'pulso_rele()' com o argumento 'a_rele2' foi alterada para função 'pulso_rele()' com o argumento 'd_rele2'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro2 = 1;
            }
          }
        } else {
          if (e_chave2 == HIGH) {
            if (quadro2 == 1) {
              pulso_rele(d_rele2); // #Observação: A função 'pulso_rele()' com o argumento 'd_rele2' foi alterada para função 'pulso_rele()' com o argumento 'a_rele2'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro2 = 0;
            }
          }
        }
//BOTAO 3---------------------------------------
        if (dataReceived[3] >= 1) {
          if (e_chave3 == HIGH) {
            if (quadro3 == 0) {
              pulso_rele(a_rele3); // #Observação: A função 'pulso_rele()' com o argumento 'a_rele3' foi alterada para função 'pulso_rele()' com o argumento 'd_rele3'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro3 = 1;
            }
          }
        } else {
          if (e_chave3 == HIGH) {
            if (quadro3 == 1) {
              pulso_rele(d_rele3); // #Observação: A função 'pulso_rele()' com o argumento 'd_rele3' foi alterada para função 'pulso_rele()' com o argumento 'a_rele3'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro3 = 0;
            }
          }
        }
//BOTAO 4--------------------------------------
        if (dataReceived[4] >= 1) {
          if (e_chave4 == HIGH) {
            if (quadro4 == 0) {
              pulso_rele(a_rele4); // #Observação: A função 'pulso_rele()' com o argumento 'a_rele4' foi alterada para função 'pulso_rele()' com o argumento 'd_rele4'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro4 = 1;
            }
          }
        } else {
          if (e_chave4 == HIGH) {
            if (quadro4 == 1) {
              pulso_rele(d_rele4); // #Observação: A função 'pulso_rele()' com o argumento 'd_rele4' foi alterada para função 'pulso_rele()' com o argumento 'a_rele4'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro4 = 0;
            }
          }
        }

//BOTAO 5-------------------------------------
        if (dataReceived[5] >= 1) {
          if (e_chave5 == HIGH) {
            if (quadro5 == 0) {
              pulso_rele(a_rele5); // #Observação: A função 'pulso_rele()' com o argumento 'a_rele5' foi alterada para função 'pulso_rele()' com o argumento 'd_rele5'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro5 = 1;
            }
          }
        } else {
          if (e_chave5 == HIGH) {
            if (quadro5 == 1) {
              pulso_rele(d_rele5); // #Observação: A função 'pulso_rele()' com o argumento 'd_rele5' foi alterada para função 'pulso_rele()' com o argumento 'a_rele5'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro5 = 0;
            }
          }
        }

//Botao 6------------------------------------      
        if (dataReceived[6] >= 1) {
          if (e_chave6 == HIGH) {
            if (quadro6 == 0) {
              pulso_rele(a_rele6); // #Observação: A função 'pulso_rele()' com o argumento 'a_rele6' foi alterada para função 'pulso_rele()' com o argumento 'd_rele6'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro6 = 1;
            }
          }
        } else {
          if (e_chave6 == HIGH) {
            if (quadro6 == 1) {
              pulso_rele(d_rele6); // #Observação: A função 'pulso_rele()' com o argumento 'd_rele6' foi alterada para função 'pulso_rele()' com o argumento 'a_rele6'. (Motivo da alteração: As luzes das quadras estavam desligando quando tentávamos ligá-las)
              quadro6 = 0;
            }
          }
        }

//----------------------------------------
      }

      break;
    }

  }
}

