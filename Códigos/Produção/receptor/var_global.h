#include <Arduino.h>

// Definicao dos reles de acionamento de cada quadro
const int a_rele1 = 8, a_rele2 = 9, a_rele3 = 10, a_rele4 = 11, a_rele5 = 12,
    a_rele6 = 14;

// Definicão dos reles de desacoionamento de cada quadro
const int d_rele1 = 2, d_rele2 = 3, d_rele3 = 4, d_rele4 = 5, d_rele5 = 6,
    d_rele6 = 7;

// Definicao das chaves para manual/automatico
const int chave1 = 13, chave2 = 15, chave3 = 16, chave4 = 17, chave5 = 18,
    chave6 = 19;

// Definicao do estado das chaves manual/automatico
int e_chave1 = 0, e_chave2 = 0, e_chave3 = 0, e_chave4 = 0, e_chave5 = 0,
    e_chave6 = 0;

// Definicao do estado dos quadros acionado/desacionado
int quadro1 = 0, quadro2 = 0, quadro3 = 0, quadro4 = 0, quadro5 = 0,
    quadro6 = 0;

// Definicao da variavel ACK
int cmd;

// Definicao do dado a ser enviado ao transmissor contendo os estados dos quadros 1,2,3,4,5
int estadoQd12345 = 0;
int estadoQd6 = 0;

byte data[4] = { 0 };
