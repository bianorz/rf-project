//#include <Arduino.h>

struct Quadro {
	int state_qd12345;
	int state_qd6;
};

void sendShiftRegister(int comando){
  digitalWrite(latch, LOW);
  shiftOut(data, clk, LSBFIRST, comando);
  shiftOut(data, clk, LSBFIRST, (comando >> 8));
  digitalWrite(latch, HIGH);
}

void set_led_value(int state_qd6, int shiftRegisterData){

  digitalWrite(qd6Man, ((state_qd6>>1)&1));
  digitalWrite(qd6Aut, (state_qd6>>2));
  digitalWrite(qd6Aci, (state_qd6&1));
  sendShiftRegister(shiftRegisterData); // Envio os dados para o shiftRegister
}

void blink_all_leds(){
  set_led_value(0b111,0b111111111111111);
  delay(250);
  set_led_value(0,0);
  delay(250);
  set_led_value(0b111,0b111111111111111);
  delay(250);
  set_led_value(0,0);
  delay(250);
}

int getEstado() {
  int a = (dataReceived[1] << 6) + (dataReceived[0]) + ((dataReceived[2]&0b111) <<12);
  return a;
}

Quadro clear_manual_blink(int state_qd12345, int state_qd6) {
  Quadro quadroLed;
  for (int i = 1; i <= 16; i = i + 3) {
    if (((state_qd12345 >> i) & 1) == 1) {

      bit_clear(state_qd12345, BIT(i-1));
    }
  }

  if (((state_qd6 >> 1) & 1) == 1) {
    bit_clear(state_qd6, BIT(0));
  }
  quadroLed.state_qd12345 = state_qd12345;
  quadroLed.state_qd6 = state_qd6;
  return quadroLed;
}

Quadro set_manual_blink(int state_qd12345, int state_qd6) {
  Quadro quadroLed;
  for (int i = 1; i <= 16; i = i + 3) {
    if (((state_qd12345 >> i) & 1) == 1) {

      bit_set(state_qd12345, BIT(i-1));
    }
  }

  if (((state_qd6 >> 1) & 1) == 1) {
    bit_set(state_qd6, BIT(0));
  }
  quadroLed.state_qd12345 = state_qd12345;
  quadroLed.state_qd6 = state_qd6;
  return quadroLed;
}



void processData(){
  if ((dataReceived[0] > 0) && (dataReceived[1] > 0) && (dataReceived[2] > 0)){
    int state_qd12345 = getEstado();
    int state_qd6 = dataReceived[2]>>3;
    //Quadro quadro = set_manual_blink(state_qd12345,state_qd6);
    //set_led_value(quadro.state_qd6,quadro.state_qd12345);
    //delay(100);
    Quadro quadro = clear_manual_blink(state_qd12345,state_qd6);
    set_led_value(quadro.state_qd6,quadro.state_qd12345);
    //delay(100);

  }
}
