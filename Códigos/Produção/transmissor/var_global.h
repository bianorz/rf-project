#include <Arduino.h>

// Definicao dos LEDs do QUADRO 6
const int
qd6Man = 5,
qd6Aut = 6,
qd6Aci = 7;

// Definicao dos botoes de acionamento
const int
bt1 = 14,
bt2 = 15,
bt3 = 16,
bt4 = 17,
bt5 = 18,
bt6 = 19;

// Definicao dos estados dos botoes
int
e_bt1 = 0,
e_bt2 = 0,
e_bt3 = 0,
e_bt4 = 0,
e_bt5 = 0,
e_bt6 = 0;

// Definicao dos pinos do registrador de Deslocamento
const int
latch = 2,
data = 3,
clk = 4;

// Definicao da variavel de cmd
byte cmd;

// Definicao da variavel que recebe os estados dos leds
int dataReceived[3] = {0};

int contador = 0;
