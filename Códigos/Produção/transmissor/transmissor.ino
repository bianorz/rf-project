// Projeto: Transmissor RF
// Autor: Bianor Galdino
// Data: 08/02/18
// Versão 0.0.2
// Descricao:
/* O projeto tem por objetivo enviar um pulso por meio da porta serial sempre que o
 * botão for pressionado. Há também uma led que informa se o pulso foi recebido com sucesso
 *
 */

/* DATA FORMAT: 4 bytes
[0xBB]: Indicate the beginning of the DATA

[]: 6 bits,  b5b4b3b2b1b0
b0: led acionado do QD1, b1: led manual do QD1, b2: led automatico do QD1
b3: led acionado do QD2, b4: led manual do QD2, b5: led automatico do QD2

[]: 6 bits,  b5b4b3b2b1b0
b0: led acionado do QD3, b1: led manual do QD3, b2: led automatico do QD3
b3: led acionado do QD4, b4: led manual do QD4, b5: led automatico do QD4

[]: 6 bits,  b5b4b3b2b1b0
b0: led acionado do QD5, b1: led manual do QD5, b2: led automatico do QD5
b3: led acionado do QD6, b4: led manual do QD6, b5: led automatico do QD6
*/


// TO DO: Add interruption for buttons

#define bit_get(p,m) ((p) & (m))
#define bit_set(p,m) ((p) |= (m))
#define bit_clear(p,m) ((p) &= ~(m))
#define bit_flip(p,m) ((p) ^= (m))
#define bit_write(c,p,m) (c ? bit_set(p,m) : bit_clear(p,m))
#define BIT(x) (0x01 << (x))
#define LONGBIT(x) ((unsigned long)0x00000001 << (x))



#include <avr/wdt.h>
#include "var_global.h"
#include "led.h"

int delay_debounce = 20;
int buttonData[8] = {};
int delaySegundosErro = 30;
int i = 0;
void setup() {


  
  // Inicializacao do Watchdog Timer
   wdt_enable(WDTO_4S);

  // Byte que indica o inicio do vetor de dados
  buttonData[0] = 0xa0;
  /* Definicao dos botoes como entrada
  bt1 = 14,
  bt2 = 15,
  bt3 = 16,
  bt4 = 17,
  bt5 = 18,
  bt6 = 19;
  */
  for (int i=14;i<=19;i++){
    pinMode(i, INPUT);
  }

  /* Definicao do quadro 6 e do registrador de deslocamento como saida
  latch = 2,
  data = 3,
  clk = 4,
  qd6Man = 5,
  qd6Aut = 6,
  qd6Aci = 7;
  */
  for (int i=2;i<=7;i++){
    pinMode(i, OUTPUT);
  }

  // Inicializacao da porta serial
  Serial.begin(9600);

  // Desligar todos os Leds
  set_led_value(0,0);

}

void loop() {
  // Resetar o watchdog timer
  wdt_reset();

//   Checar botao 1
  e_bt1 = digitalRead(bt1);

  // Se pressionado, enviar comando para acionar o quadro 1
  if (e_bt1 == HIGH) {
    buttonData[1] = 0xFF;
    delay(delay_debounce);
  } else {
    buttonData[1] = 0;
    delay(delay_debounce);
  }

  // Checar botao 2
  e_bt2 = digitalRead(bt2);

  // Se pressionado, enviar comando para acionar o quadro 2
  if (e_bt2 == HIGH) {
    buttonData[2] = 0xFF;
    delay(delay_debounce);
  } else {
    buttonData[2] = 0;
    delay(delay_debounce);
  }

  // Checar botao 3
  e_bt3 = digitalRead(bt3);

  // Se pressionado, enviar comando para acionar o quadro 3
  if (e_bt3 == HIGH) {
    buttonData[3] = 0xFF;
    delay(delay_debounce);
  } else {
    buttonData[3] = 0;
    delay(delay_debounce);
  }

  // Checar botao 4
  e_bt4 = digitalRead(bt4);

  // Se pressionado, enviar comando para acionar o quadro 4
  if (e_bt4 == HIGH) {
    buttonData[4] = 0xFF;
    delay(delay_debounce);
  } else {
    buttonData[4] = 0;
    delay(delay_debounce);
  }

  // Checar botao 5
  e_bt5 = digitalRead(bt5);

  // Se pressionado, enviar comando para acionar o quadro 5
  if (e_bt5 == HIGH) {
    buttonData[5] = 0xFF;
    delay(delay_debounce);
  } else {
    buttonData[5] = 0;
    delay(delay_debounce);
  }

 // Checar botao 6
  e_bt6 = digitalRead(bt6);

  // Se pressionado, enviar comando para acionar o quadro 6
  if (e_bt6 == HIGH) {
    buttonData[6] = 0xFF;
    delay(delay_debounce);
  } else {
    buttonData[6] = 0;
    delay(delay_debounce);
  }

  // Calculo do checksum
   buttonData[7] = (buttonData[6] >= 1) + (buttonData[5] >= 1) + (buttonData[4] >= 1) + 
                   (buttonData[3] >= 1) + (buttonData[2] >= 1) + (buttonData[1] >= 1); 

  // Enviar Dados

  for (i=0;i<=7;i++) {
    Serial.write(buttonData[i]);
  }
  // Necessário delay de 50ms antes de ler a serial
  delay(50);
  // Checar estado do receptor
  contador++;
  if (contador >= int((delaySegundosErro*1000)/(6*delay_debounce+50))){
    blink_all_leds();
    contador = 0;
  }

    // Checar comandos do receptor
  while(Serial.available() > 0) {

    cmd = Serial.read();
    contador = 0;
        // Comparar os diferentes comandos possíveis
    switch (cmd) {

     case 0xcc:
      set_led_value(0,0); // Desligar todos os leds
      delay(5);
      break;
     case 0xbb:
      dataReceived[0] = Serial.read();
      dataReceived[1] = Serial.read();
      dataReceived[2] = Serial.read();
      processData();
      break;

    }
  }

}

